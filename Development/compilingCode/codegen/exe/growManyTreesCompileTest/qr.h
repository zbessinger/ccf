/*
 * File: qr.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __QR_H__
#define __QR_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void qr(const emxArray_real_T *A, emxArray_real_T *Q, emxArray_real_T *R,
               emxArray_real_T *E);

#endif

/*
 * File trailer for qr.h
 *
 * [EOF]
 */
