/*
 * File: bsxfun.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __BSXFUN_H__
#define __BSXFUN_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void b_bsxfun(const emxArray_real_T *a, const emxArray_real_T *b,
                     emxArray_boolean_T *c);
extern void bsxfun(double a, const double b[2], boolean_T c[2]);
extern void c_bsxfun(const emxArray_real_T *a, const emxArray_real_T *b,
                     emxArray_boolean_T *c);
extern void d_bsxfun(const emxArray_real_T *a, const emxArray_real_T *b,
                     emxArray_real_T *c);
extern void e_bsxfun(const emxArray_boolean_T *a, const emxArray_real_T *b,
                     emxArray_real_T *c);
extern void f_bsxfun(const emxArray_real_T *a, const emxArray_real_T *b,
                     emxArray_real_T *c);
extern void g_bsxfun(const emxArray_real_T *a, const emxArray_real_T *b,
                     emxArray_real_T *c);
extern void h_bsxfun(const emxArray_real_T *a, const emxArray_real_T *b,
                     emxArray_real_T *c);

#endif

/*
 * File trailer for bsxfun.h
 *
 * [EOF]
 */
