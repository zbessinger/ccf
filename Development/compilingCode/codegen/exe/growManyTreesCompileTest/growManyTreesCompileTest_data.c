/*
 * File: growManyTreesCompileTest_data.c
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

/* Include files */
#include "rt_nonfinite.h"
#include "growManyTreesCompileTest.h"
#include "growManyTreesCompileTest_data.h"

/* Variable Definitions */
boolean_T state_not_empty;

/*
 * File trailer for growManyTreesCompileTest_data.c
 *
 * [EOF]
 */
