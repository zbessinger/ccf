/*
 * File: randperm.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:37:59
 */

#ifndef __RANDPERM_H__
#define __RANDPERM_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void randperm(double n, double k, emxArray_real_T *p);

#endif

/*
 * File trailer for randperm.h
 *
 * [EOF]
 */
