/*
 * File: queryIfOnlyTwoUniqueRows.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:37:59
 */

#ifndef __QUERYIFONLYTWOUNIQUEROWS_H__
#define __QUERYIFONLYTWOUNIQUEROWS_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern boolean_T queryIfOnlyTwoUniqueRows(const emxArray_real_T *X);

#endif

/*
 * File trailer for queryIfOnlyTwoUniqueRows.h
 *
 * [EOF]
 */
