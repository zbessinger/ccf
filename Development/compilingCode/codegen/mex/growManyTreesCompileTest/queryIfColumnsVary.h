/*
 * queryIfColumnsVary.h
 *
 * Code generation for function 'queryIfColumnsVary'
 *
 */

#ifndef __QUERYIFCOLUMNSVARY_H__
#define __QUERYIFCOLUMNSVARY_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void queryIfColumnsVary(const emlrtStack *sp, const emxArray_real_T
  *XvarToTest, real_T tol, emxArray_boolean_T *bVar);

#endif

/* End of code generation (queryIfColumnsVary.h) */
