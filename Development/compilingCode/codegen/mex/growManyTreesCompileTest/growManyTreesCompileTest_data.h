/*
 * growManyTreesCompileTest_data.h
 *
 * Code generation for function 'growManyTreesCompileTest_data'
 *
 */

#ifndef __GROWMANYTREESCOMPILETEST_DATA_H__
#define __GROWMANYTREESCOMPILETEST_DATA_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Variable Declarations */
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern emlrtRSInfo c_emlrtRSI;
extern emlrtRSInfo r_emlrtRSI;
extern emlrtRSInfo s_emlrtRSI;
extern emlrtRSInfo t_emlrtRSI;
extern emlrtRSInfo u_emlrtRSI;
extern emlrtRSInfo v_emlrtRSI;
extern emlrtRSInfo w_emlrtRSI;
extern emlrtRSInfo x_emlrtRSI;
extern emlrtRSInfo oe_emlrtRSI;
extern emlrtRSInfo ue_emlrtRSI;
extern emlrtRSInfo df_emlrtRSI;
extern emlrtRSInfo ef_emlrtRSI;
extern emlrtRSInfo ff_emlrtRSI;
extern emlrtRSInfo gf_emlrtRSI;
extern emlrtRSInfo nf_emlrtRSI;
extern emlrtRSInfo of_emlrtRSI;
extern emlrtRSInfo pf_emlrtRSI;
extern emlrtRSInfo qf_emlrtRSI;
extern emlrtRSInfo wf_emlrtRSI;
extern emlrtRSInfo bg_emlrtRSI;
extern emlrtRSInfo cg_emlrtRSI;
extern emlrtRSInfo fg_emlrtRSI;
extern emlrtRSInfo gg_emlrtRSI;
extern emlrtRSInfo ig_emlrtRSI;
extern emlrtRSInfo jg_emlrtRSI;
extern emlrtRSInfo kg_emlrtRSI;
extern emlrtRSInfo hh_emlrtRSI;
extern emlrtRSInfo ih_emlrtRSI;
extern emlrtRSInfo jh_emlrtRSI;
extern emlrtRSInfo kh_emlrtRSI;
extern emlrtRSInfo bi_emlrtRSI;
extern emlrtRSInfo ci_emlrtRSI;
extern emlrtRSInfo di_emlrtRSI;
extern emlrtRSInfo ei_emlrtRSI;
extern emlrtRSInfo fi_emlrtRSI;
extern emlrtRSInfo gi_emlrtRSI;
extern emlrtRSInfo hi_emlrtRSI;
extern emlrtRSInfo ii_emlrtRSI;
extern emlrtRSInfo ji_emlrtRSI;
extern emlrtRSInfo ki_emlrtRSI;
extern emlrtRSInfo li_emlrtRSI;
extern emlrtRSInfo mi_emlrtRSI;
extern emlrtRSInfo ni_emlrtRSI;
extern emlrtRSInfo oi_emlrtRSI;
extern emlrtRSInfo pi_emlrtRSI;
extern emlrtRSInfo qi_emlrtRSI;
extern emlrtRSInfo ri_emlrtRSI;
extern emlrtRSInfo ui_emlrtRSI;
extern emlrtRSInfo vi_emlrtRSI;
extern emlrtRSInfo xi_emlrtRSI;
extern emlrtRSInfo yi_emlrtRSI;
extern emlrtRSInfo aj_emlrtRSI;
extern emlrtRSInfo bj_emlrtRSI;
extern emlrtRSInfo fj_emlrtRSI;
extern emlrtRSInfo gj_emlrtRSI;
extern emlrtRSInfo hj_emlrtRSI;
extern emlrtRSInfo ij_emlrtRSI;
extern emlrtRSInfo jj_emlrtRSI;
extern emlrtRSInfo kj_emlrtRSI;
extern emlrtRSInfo lj_emlrtRSI;
extern emlrtRSInfo mj_emlrtRSI;
extern emlrtRSInfo oj_emlrtRSI;
extern emlrtRSInfo pj_emlrtRSI;
extern emlrtRSInfo qj_emlrtRSI;
extern emlrtRSInfo rj_emlrtRSI;
extern emlrtRSInfo sj_emlrtRSI;
extern emlrtRSInfo tj_emlrtRSI;
extern emlrtRSInfo uj_emlrtRSI;
extern emlrtRSInfo vj_emlrtRSI;
extern emlrtRSInfo nm_emlrtRSI;
extern emlrtRSInfo om_emlrtRSI;
extern emlrtRSInfo ho_emlrtRSI;
extern emlrtRSInfo io_emlrtRSI;
extern emlrtRSInfo jo_emlrtRSI;
extern emlrtMCInfo emlrtMCI;
extern emlrtMCInfo b_emlrtMCI;
extern emlrtMCInfo c_emlrtMCI;
extern emlrtMCInfo d_emlrtMCI;
extern emlrtMCInfo g_emlrtMCI;
extern emlrtMCInfo h_emlrtMCI;
extern emlrtMCInfo o_emlrtMCI;
extern emlrtMCInfo y_emlrtMCI;
extern emlrtMCInfo ab_emlrtMCI;
extern emlrtMCInfo bb_emlrtMCI;
extern emlrtMCInfo hb_emlrtMCI;
extern emlrtMCInfo ib_emlrtMCI;
extern emlrtMCInfo jb_emlrtMCI;
extern emlrtMCInfo kb_emlrtMCI;
extern emlrtMCInfo lb_emlrtMCI;
extern emlrtMCInfo xb_emlrtMCI;
extern emlrtMCInfo yb_emlrtMCI;
extern emlrtRTEInfo f_emlrtRTEI;
extern emlrtRTEInfo g_emlrtRTEI;
extern emlrtRTEInfo h_emlrtRTEI;
extern emlrtRTEInfo j_emlrtRTEI;
extern emlrtRTEInfo mb_emlrtRTEI;
extern emlrtRTEInfo sc_emlrtRTEI;
extern emlrtRTEInfo uc_emlrtRTEI;
extern emlrtRTEInfo xc_emlrtRTEI;
extern emlrtRTEInfo yc_emlrtRTEI;
extern emlrtRTEInfo bd_emlrtRTEI;
extern emlrtBCInfo hc_emlrtBCI;
extern emlrtDCInfo q_emlrtDCI;
extern emlrtBCInfo ff_emlrtBCI;
extern emlrtBCInfo gf_emlrtBCI;
extern emlrtBCInfo jf_emlrtBCI;
extern emlrtECInfo qb_emlrtECI;
extern emlrtBCInfo lf_emlrtBCI;
extern emlrtBCInfo mf_emlrtBCI;
extern c_growManyTreesCompileTestStack *d_growManyTreesCompileTestStack;
extern emlrtRSInfo so_emlrtRSI;
extern emlrtRSInfo uo_emlrtRSI;
extern emlrtRSInfo yo_emlrtRSI;
extern emlrtRSInfo ap_emlrtRSI;
extern emlrtRSInfo cp_emlrtRSI;
extern emlrtRSInfo dp_emlrtRSI;
extern emlrtRSInfo ep_emlrtRSI;
extern emlrtRSInfo fp_emlrtRSI;
extern emlrtRSInfo gp_emlrtRSI;
extern emlrtRSInfo hp_emlrtRSI;
extern emlrtRSInfo mp_emlrtRSI;
extern emlrtRSInfo wp_emlrtRSI;
extern emlrtRSInfo aq_emlrtRSI;
extern emlrtRSInfo bq_emlrtRSI;
extern emlrtRSInfo cq_emlrtRSI;
extern emlrtRSInfo gq_emlrtRSI;
extern emlrtRSInfo hq_emlrtRSI;

#endif

/* End of code generation (growManyTreesCompileTest_data.h) */
