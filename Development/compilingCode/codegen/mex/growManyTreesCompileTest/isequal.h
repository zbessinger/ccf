/*
 * isequal.h
 *
 * Code generation for function 'isequal'
 *
 */

#ifndef __ISEQUAL_H__
#define __ISEQUAL_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern boolean_T b_isequal(const emxArray_boolean_T *varargin_1);
extern boolean_T isequal(const emxArray_boolean_T *varargin_1);

#endif

/* End of code generation (isequal.h) */
