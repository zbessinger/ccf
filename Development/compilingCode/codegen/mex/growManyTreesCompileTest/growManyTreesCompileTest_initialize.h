/*
 * growManyTreesCompileTest_initialize.h
 *
 * Code generation for function 'growManyTreesCompileTest_initialize'
 *
 */

#ifndef __GROWMANYTREESCOMPILETEST_INITIALIZE_H__
#define __GROWMANYTREESCOMPILETEST_INITIALIZE_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void growManyTreesCompileTest_initialize(emlrtContext *aContext);

#endif

/* End of code generation (growManyTreesCompileTest_initialize.h) */
