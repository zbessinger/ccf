/*
 * _coder_growManyTreesCompileTest_api.h
 *
 * Code generation for function '_coder_growManyTreesCompileTest_api'
 *
 */

#ifndef ___CODER_GROWMANYTREESCOMPILETEST_API_H__
#define ___CODER_GROWMANYTREESCOMPILETEST_API_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void growManyTreesCompileTest_api(c_growManyTreesCompileTestStack *SD,
  const mxArray * const prhs[5], const mxArray *plhs[1]);

#endif

/* End of code generation (_coder_growManyTreesCompileTest_api.h) */
