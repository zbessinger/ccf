/*
 * growManyTreesCompileTest_terminate.h
 *
 * Code generation for function 'growManyTreesCompileTest_terminate'
 *
 */

#ifndef __GROWMANYTREESCOMPILETEST_TERMINATE_H__
#define __GROWMANYTREESCOMPILETEST_TERMINATE_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void growManyTreesCompileTest_atexit(void);
extern void growManyTreesCompileTest_terminate(void);

#endif

/* End of code generation (growManyTreesCompileTest_terminate.h) */
